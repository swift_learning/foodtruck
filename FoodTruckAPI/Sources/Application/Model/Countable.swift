//
//  Countable.swift
//  Application
//
//  Created by iulian david on 16/05/2019.
//

struct Countable: Codable {
    /// ID
    public let count: Int

    init(_ count: Int) {
        self.count = count
    }
}
