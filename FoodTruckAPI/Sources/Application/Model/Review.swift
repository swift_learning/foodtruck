//
//  Review.swift
//  Application
//
//  Created by iulian david on 23/10/2019.
//

import CouchDB

// swiftlint:disable identifier_name
struct Review: CustomModel {
    var type: String = "review"

    // CouchDB specific
    let _id: String?
    // CouchDB specific
    var _rev: String?

    /// FoodTruckId
    /// - ID of the Food Tuck this review belongs to
    public var truckId: String?

    /// Title of Review
    public let reviewTitle: String?

    /// Review Text
    public let reviewText: String?

    /// Star Rating
    /// 1-5
    public let starRating: Int

    public enum CodingKeys: String, CodingKey {
        case type
        case _id
        case _rev
        case truckId = "truckid"
        case reviewTitle = "reviewtitle"
        case reviewText = "reviewtext"
        case starRating = "starrating"
    }

    init?(_id: String? = nil, truckId: String?, reviewTitle: String?, reviewText: String?, starRating: Int?) {
        guard let rating = starRating else { return nil }
        self.truckId = truckId
        self.reviewText = reviewText
        self.reviewTitle = reviewTitle
        self.starRating = rating
        self._id = _id
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _id = try? container.decode(String.self, forKey: ._id)
        _rev = try? container.decode(String.self, forKey: ._rev)
        truckId = try? container.decode(String.self, forKey: .truckId)
        reviewTitle = try? container.decode(String.self, forKey: .reviewTitle)
        reviewText = try? container.decode(String.self, forKey: .reviewText)
        starRating = try container.decode(Int.self, forKey: .starRating)

    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(truckId, forKey: .truckId)
        try container.encode(reviewTitle, forKey: .reviewTitle)
        try container.encode(reviewText, forKey: .reviewText)
        try container.encode(starRating, forKey: .starRating)
        try container.encode(type, forKey: .type)
        if let id = _id {
            try container.encode(id, forKey: ._id)
        }
    }
}

extension Review: Equatable {
    public static func == (lhs: Review, rhs: Review) -> Bool {
        return lhs._id == rhs._id &&
            lhs.truckId == rhs.truckId &&
            lhs.reviewTitle == rhs.reviewTitle &&
            lhs.reviewText == rhs.reviewText &&
            lhs.starRating == rhs.starRating
    }
}
