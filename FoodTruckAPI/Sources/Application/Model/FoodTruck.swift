//
//  FoodTruckItem.swift
//  Application
//
//  Created by iulian david on 12/05/2019.
//

// 1
import CouchDB

// swiftlint:disable identifier_name
struct FoodTruck: CustomModel {
    var type: String = "foodtruck"

    // CouchDB specific
    let _id: String?
    // CouchDB specific
    var _rev: String?

    /// name of the food truck Business
    public let name: String?

    ///Food Type
    public let foodType: String?

    ///Average cost of the food served
    public let avgCost: Float?

    ///Coordinate latitude
    public let latitude: Float?

    ///Coordinate longitude
    public let longitude: Float?

    public enum DecodingKeys: String, CodingKey {
        case type
        case name
        case foodType = "foodtype"
        case avgCost = "avgcost"
        case latitude
        case longitude
        case _id
        case _rev
    }

    public enum EncodingKeys: String, CodingKey {
        case type
        case name
        case foodType = "foodtype"
        case avgCost = "avgcost"
        case latitude
        case longitude
        case _id
    }

    public init?(_id: String? = nil, name: String? = nil, foodType: String? = nil, avgCost: Float? = nil, latitude: Float? = nil, longitude: Float? = nil) {
        if _id == nil, name == nil, foodType == nil, avgCost == nil,
            latitude == nil, longitude == nil {
            return nil
        }
        self._id = _id
        self.name = name
        self.foodType = foodType
        self.avgCost = avgCost
        self.latitude = latitude
        self.longitude = longitude
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DecodingKeys.self)

        name = try? container.decode(String.self, forKey: .name)

        foodType = try? container.decode(String.self, forKey: .foodType)

        _id = try? container.decode(String.self, forKey: ._id)

        _rev = try? container.decode(String.self, forKey: ._rev)

        avgCost = try? container.decode(Float.self, forKey: .avgCost)

        latitude = try? container.decode(Float.self, forKey: .latitude)

        longitude = try? container.decode(Float.self, forKey: .longitude)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: EncodingKeys.self)
        do {
            try container.encode(name, forKey: .name)
            try container.encode(type, forKey: .type)
            try container.encode(foodType, forKey: .foodType)
            if let id = _id {
                try container.encode(id, forKey: ._id)
            }
            try container.encode(avgCost, forKey: .avgCost)
            try container.encode(latitude, forKey: .latitude)
            try container.encode(longitude, forKey: .longitude)
        } catch let err {
            print("Error while decoding", err)
        }

    }
}

extension FoodTruck: Equatable {
    public static func == (lhs: FoodTruck, rhs: FoodTruck) -> Bool {
        return lhs._id == rhs._id &&
            lhs.name == rhs.name &&
            lhs.foodType == rhs.foodType &&
            lhs.avgCost == rhs.avgCost &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude
    }
}
