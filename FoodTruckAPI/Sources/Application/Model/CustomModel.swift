//
//  CustomModel.swift
//  Application
//
//  Created by iulian david on 26/05/2019.
//

import CouchDB

protocol CustomModel: Document {
    /// Type
    var type: String { get set }
}
