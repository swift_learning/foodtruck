import Foundation
import Kitura
import LoggerAPI
import Configuration
import CloudEnvironment
import KituraContracts
import Health

public let projectPath = ConfigurationManager.BasePath.project.path
public let health = Health()

public class App {
    let router = Router()
    let cloudEnv = CloudEnv()

    public init() throws {
        // Run the metrics initializer
        initializeMetrics(router: router)
    }

    func postInit() throws {
        // Endpoints
        initializeHealthRoutes(app: self)
        router.get("/") { _, response, _ in
            response.send("Hi you! Have a nice day.")
        }

    }

    public func run() throws {
        try postInit()

        let trucks: APIGateway
        do {
            Log.info("Attempting init with CF environment")
            let credentials = try getConfig()
            Log.info("Init with VCAP_SERVICES credentials")
            trucks = APIGateway(credentials: credentials)
        } catch {
            Log.info("Could not retrieve CF env: init  with defaults")
            trucks = APIGateway()
        }

        _ = FoodTruckController(router: router, backend: trucks)

        Kitura.addHTTPServer(onPort: cloudEnv.port, with: router)
        Kitura.run()
    }

    func getConfig() throws -> [String: Any] {

        let manager = ConfigurationManager()
        manager.load(.environmentVariables)
        ///
        if let credentials = manager["VCAP_SERVICES:cloudantNoSQLDB:0:credentials"] as? [String: Any] {
            return credentials
        }
        /// Read data from config/xxxx.json
        guard let cloudCreds = cloudEnv.getDictionary(name: "Cloudant NoSQL DB") else { throw ConfigError() }

        return cloudCreds
    }

}

struct ConfigError: LocalizedError {
    var errorDescription: String? {
        return "Could not retrieve config info"
    }
}
