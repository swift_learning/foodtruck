//
//  ReviewAPI.swift
//  Application
//
//  Created by iulian david on 23/10/2019.
//

import Foundation

protocol ReviewAPI {
    func addReview(_ review: Review, completion:@escaping  (Review?, Error?) -> Void)

    func getReviews(for truckId: String, completion:@escaping  ([Review]?, Error?) -> Void)

    func getReview(docId: String, completion:@escaping  (Review?, Error?) -> Void)

    func updateReview(docId: String, with newReview: Review, completion:@escaping  (Review?, Error?) -> Void)

    func deleteReview(docId: String, completion:@escaping  (Error?) -> Void)

    func deleteReviews(for truckId: String, completion: @escaping (Swift.Error?) -> Void)

    func countReviews(for truckId: String?, completion:@escaping  (Int?, Error?) -> Void)

    func countReviews(completion:@escaping  (Int?, Error?) -> Void)

    func averageRating(for truckId: String, completion:@escaping  (Int?, Error?) -> Void)
}
