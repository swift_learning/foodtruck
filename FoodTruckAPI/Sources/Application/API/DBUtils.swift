//
//  Uitls.swift
//  A clone of CouchDBUtils so that the views created on CouchDB execute
//
//  Created by iulian david on 22/10/2019.
//

import Foundation
import CouchDB
import KituraNet

class DBUtils {
    private static let customAllowedCharacterSet =  NSCharacterSet(charactersIn: "\"#%/<>?@\\^`{|}+ ").inverted

    // Applies percent-encoding to a string, suitable for use as a CouchDB document path. In addition to characters normally encoded
    // in a URL path, instances of the + character are also percent-encoded.

    static func escape(url: String) -> String {
        if let escaped = url.addingPercentEncoding(withAllowedCharacters: customAllowedCharacterSet) {
            return escaped
        }
        return url
    }

    static func createQueryParamForArray(_ array: [Any]) -> String {
        var result = "["
        var comma = ""
        for element in array {
            if let item = element as? String {
                result += "\(comma)\"\(escape(url: item))\""
            } else {
                let objMirror = Mirror(reflecting: element)
                if objMirror.subjectType == NSObject.self {
                    result += "\(comma){}"
                } else {
                    result += "\(comma)\(element)"
                }
            }
            comma = ","
        }
        return result + "]"
    }

    // swiftlint:disable all
    public static func executeQueryOnView(requestOptions: [ClientRequest.Options], usingDbName dbName: String,
                                          usingDesign designName: String,
                                          for view: String, usingParameters params: [Database.QueryParameters] = [],
                                          completion: @escaping ([String: Any]?, Swift.Error?) -> Void) {
        var mutableRequestOptions = requestOptions

        var paramString = ""
        var keys: [Any]?

        for param in params {
            switch param {
            case .conflicts (let value):
                paramString += "conflicts=\(value)&"
            case .descending (let value):
                paramString += "descending=\(value)&"
            case .endKey (let value):
                if value.count == 1 {
                    if let endKey = value[0] as? String {
                        paramString += "endkey=\"\(DBUtils.escape(url: endKey))\"&"
                    } else {
                        paramString += "endkey=\(value[0])&"
                    }
                } else {
                    paramString += "endkey=" + DBUtils.createQueryParamForArray(value) + "&"
                }
            case .endKeyDocID (let value):
                paramString += "endkey_docid=\"\(DBUtils.escape(url: value))\"&"
            case .group (let value):
                paramString += "group=\(value)&"
            case .groupLevel (let value):
                paramString += "group_level=\(value)&"
            case .includeDocs (let value):
                paramString += "include_docs=\(value)&"
            case .attachments (let value):
                paramString += "attachments=\(value)&"
            case .attachmentEncodingInfo (let value):
                paramString += "att_encoding_info=\(value)&"
            case .inclusiveEnd (let value):
                paramString += "inclusive_end=\(value)&"
            case .limit (let value):
                paramString += "limit=\(value)&"
            case .reduce (let value):
                paramString += "reduce=\(value)&"
            case .skip (let value):
                paramString += "skip=\(value)&"
            case .stale (let value):
                paramString += "stale=\"\(value)\"&"
            case .startKey (let value):
                if value.count == 1 {
                    if let startKey = value[0] as? String {
                        paramString += "startkey=\"\(DBUtils.escape(url: startKey))\"&"
                    } else {
                        paramString += "startkey=\(value[0])&"
                    }
                } else {
                    paramString += "startkey=" + DBUtils.createQueryParamForArray(value) + "&"
                }
            case .startKeyDocID (let value):
                paramString += "start_key_doc_id=\"\(DBUtils.escape(url: value))\"&"
            case .updateSequence (let value):
                paramString += "update_seq=\(value)&"
            case .keys (let value):
                if value.count == 1 {
                    if value[0] is String {
                        paramString += "key=\"\(DBUtils.escape(url: value[0] as! String))\"&"
                    } else if let anyArray = value[0] as? [Any] {
                        paramString += "key=" + DBUtils.createQueryParamForArray(anyArray) + "&"
                    }
                } else {
                    keys = value
                }
            }
        }

        if paramString.count > 0 {
            paramString = "?" + String(paramString.dropLast())
        }

        var method = "GET"
        var hasBody = false
        let body: [String: Any]?
        if let keys = keys {
            method = "POST"
            hasBody = true
            body = ["keys": keys]
        } else {
            body = nil
        }

        mutableRequestOptions.append(.method(method))

        var headers = [String: String]()
        headers["Accept"] = "application/json"
        if hasBody {
            headers["Content-Type"] = "application/json"
        }
        mutableRequestOptions.append(.headers(headers))

        mutableRequestOptions.append(.path("\(dbName)/_design/\(designName)/_view/\(view)\(paramString)"))
        let req = HTTP.request(mutableRequestOptions) { response in
            if let response = response {
                guard response.statusCode == HTTPStatusCode.OK else {

                    return completion(nil, APIControllerError.dbError(reason: "unable to authenticate"))
                }
                do {
                    var body = Data()
                    try response.readAllData(into: &body)
                    let bodyJSON = (try? JSONSerialization.jsonObject(with: body, options: [])) as? [String: Any]
                    return completion(bodyJSON, nil)
                } catch {
                    //Log this exception
                }

            } else {
                return completion(nil, APIControllerError.dbError(reason: "unable to authenticate"))
            }

        }
        if let body = body, let bodyAsData = try? JSONSerialization.data(withJSONObject: body, options: []) {
            req.end(bodyAsData)
        } else {
            req.end()
        }
    }

}
