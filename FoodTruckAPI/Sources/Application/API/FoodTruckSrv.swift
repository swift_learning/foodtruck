//
//  FoodTruckSrv.swift
//  Application
//
//  Created by iulian david on 26/10/2019.
//

import Foundation
import CouchDB
import LoggerAPI
import KituraNet

class FoodTruckSrv: FoodTruckAPI {

    let database: Database
    var requestOptions: [ClientRequest.Options]
    let designName = "foodtruckdesign"
    let dbName: String

    init(db: Database, requestOptions: [ClientRequest.Options], dbName: String) {
        self.database = db
        self.requestOptions = requestOptions
        self.dbName = dbName
    }

   // Add Food Truck
    public func addTruck(_ truck: FoodTruck, completion: @escaping (FoodTruck?, Swift.Error?) -> Void) {
        guard truck.name != nil, truck.foodType != nil,
            truck.avgCost != nil, truck.longitude != nil,
            truck.latitude != nil else {
                completion(nil, APIControllerError.parseError)
                return
        }

        database.create(truck) { (document, err) in
            if let docId = document?.id {
                self.database.retrieve(docId, callback: completion)
            } else {
                completion(nil, err)
            }
        }
    }

    // Get specific Food Truck
    public func getTruck(docId: String, completion: @escaping (FoodTruck?, Swift.Error?) -> Void) {
        database.retrieve(docId, callback: completion)
    }

    // Update specific Food Truck
    func updateTruck(docId: String, with newTruckData: FoodTruck, completion: @escaping (FoodTruck?, Swift.Error?) -> Void) {
        getTruck(docId: docId) { response, err in
            guard let truck = response, let rev = truck._rev else {
                Log.error("Error retrieving <docID> \(docId) : \(String(describing: err))")
                return completion(nil, err)
            }

            guard let updatedTruck = FoodTruck(_id: truck._id, name: newTruckData.name ?? truck.name, foodType: newTruckData.foodType ?? truck.foodType,
                                                   avgCost: newTruckData.avgCost ?? truck.avgCost, latitude: newTruckData.latitude ?? truck.latitude,
                                                   longitude: newTruckData.longitude ?? truck.longitude) else {
                                                    Log.error("Error retrieving <docID> \(docId) : \(String(describing: err))")
                                                    return completion(nil, APIControllerError.parseError)
            }

            self.database.update(docId, rev: rev, document: updatedTruck, callback: { (_, error) in
                guard error == nil else {
                    Log.error("Error: \(error!) updating <document>: \(updatedTruck))")
                    return completion(nil, error!)
                }
                completion(updatedTruck, nil)
            })
        }
    }

    func getAllTrucks(completion: @escaping ([FoodTruck]?, Swift.Error?) -> Void) {
        database.queryByView("all_trucks", ofDesign: designName, usingParameters: [.descending(true), .includeDocs(true)], callback: { (docs, error) in
            guard let documents = docs else {
                Log.error("Error retrieving all documents: \(String(describing: error))")
                return completion(nil, error)
            }
            let trucks = documents.decodeDocuments(ofType: FoodTruck.self)
            completion(trucks, nil)
        })
    }

    // Clear All Food Trucks
    func clearAll(  completion: @escaping (Swift.Error?) -> Void) {

        database.queryByView("all_documents", ofDesign: "foodtruckdesign", usingParameters: [.descending(true), .includeDocs(true)]) { (doc, err) in
            guard let doc = doc else {
                completion(err)
                return
            }

            guard let idAndRev = try? self.getIdAndRev(doc) else {
                completion(err)
                return
            }

            if idAndRev.count == 0 {
                completion(nil)
            } else {
                for index in 0...idAndRev.count - 1 {
                    let truck = idAndRev[index]
                    if let docId = truck._id, let rev = truck._rev {
                        self.database.delete(docId, rev: rev, callback: { (err) in
                            completion(err)
                            return
                        })
                    }
                    completion(nil)
                }

            }
        }

    }

    // Get count of all Food Trucks
    public func countTrucks(completion: @escaping (Int?, Swift.Error?) -> Void) {
        DBUtils.executeQueryOnView(requestOptions: requestOptions, usingDbName: dbName, usingDesign: designName, for: "total_trucks") { (result, error) in
            if let doc = result, error == nil {
                if let count = (doc["rows"] as? [[String: Any]])?.first?["value"] as? Int {
                    completion(count, nil)
                } else {
                    completion(0, nil)
                }
            } else {
                completion(nil, error)
            }
        }
    }

    private func getIdAndRev(_ document: AllDatabaseDocuments) throws -> [IdAndRevDoc] {
        return document.decodeDocuments(ofType: IdAndRevDoc.self)
    }

    func deleteTruck(docId: String, completion: @escaping (Swift.Error?) -> Void) {
        getTruck(docId: docId) { response, err in
            guard let truck = response, let rev = truck._rev else {
                Log.error("Error retrieving <docID> \(docId) : \(String(describing: err))")
                return completion(err)
            }

            self.database.delete(docId, rev: rev, callback: { (err) in
                return completion(err)
            })

        }
    }

}

// swiftlint:disable identifier_name
private struct IdAndRevDoc: Document {
    // 1
    let _id: String?
    // 2
    var _rev: String?
}
