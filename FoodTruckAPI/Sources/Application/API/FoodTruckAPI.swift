//
//  FoodTruckAPI.swift
//  Application
//
//  Created by iulian david on 11/05/2019.
//

import Foundation

protocol FoodTruckAPI {

    // MARK: - Trucks
    // Get all Food Trucks
    func getAllTrucks(completion: @escaping ([FoodTruck]?, Error?) -> Void)

    // Add Food Truck
    func addTruck(_ truck: FoodTruck, completion: @escaping (FoodTruck?, Error?) -> Void)

    // Get specific Food Truck
    func getTruck(docId: String, completion: @escaping (FoodTruck?, Error?) -> Void)

    // Update Food Truck
    func updateTruck(docId: String, with: FoodTruck, completion: @escaping (FoodTruck?, Error?) -> Void)

    // Clear All Food Trucks
    func clearAll(completion: @escaping (Swift.Error?) -> Void)

    // Get count of all Food Trucks
    func countTrucks(completion: @escaping (Int?, Error?) -> Void)

    func deleteTruck(docId: String, completion: @escaping (Error?) -> Void)
}
