//
//  FoodTruck.swift
//  Application
//
//  Created by iulian david on 11/05/2019.
//

import Foundation
import LoggerAPI
import CouchDB
import KituraNet

#if os(Linux)
typealias ValueType = Any
#else
typealias ValueType = AnyObject
#endif

public enum APIControllerError: Swift.Error {
    case parseError
    case authError
    case dbError(reason: String)
}

class APIGateway: FoodTruckAPI, ReviewAPI {

    public static let defaultDBHost = "localhost"
    public static let defaultDBPort = UInt16(5984)
    public static let defaultDBName = "foodtruckapi"
    public static let defaultDBUsername = "iuli"
    public static let defaultDBPassword = "123456"

    let dbName: String
    let designName = "foodtruckdesign"
    let connectionProps: ConnectionProperties
    var couchDB: Database?

    var requestOptions: [ClientRequest.Options] = []
    var foodtruckApi: FoodTruckAPI!
    var reviewApi: ReviewAPI!

    // MARK: - DB RELATED
    public init(database: String = APIGateway.defaultDBName, host: String = APIGateway.defaultDBHost,
                port: UInt16 = APIGateway.defaultDBPort, usermane: String? = APIGateway.defaultDBUsername, password: String? = APIGateway.defaultDBPassword) {

        //if in Bluemix the default protocol is https, else http
        let secured = (host == APIGateway.defaultDBHost) ? false : true
        connectionProps = ConnectionProperties(host: host, port: port, secured: secured, username: usermane, password: password)
        dbName = database
        setupDb()
        buildRequestOptions(host: host, port: port, secured: secured, username: usermane, password: password)
        foodtruckApi = FoodTruckSrv(db: couchDB!, requestOptions: requestOptions, dbName: dbName)
        reviewApi = ReviewAPISrv(db: couchDB!, requestOptions: requestOptions, dbName: dbName)
    }

    private func buildRequestOptions(host: String, port: UInt16, secured: Bool, username: String?, password: String?) {
        if let username = username {
            requestOptions.append(.username(username))
        }
        if let password = password {
            requestOptions.append(.password(password))
        }
        requestOptions.append(.schema(secured ? "https://" : "http://"))
        requestOptions.append(.hostname(host))
        requestOptions.append(.port(Int16(bitPattern: port)))
        requestOptions.append(.method("GET"))
        var headers = [String: String]()
        headers["Accept"] = "application/json"
        requestOptions.append(.headers(headers))
    }

    public convenience init (credentials: [String: Any]) {
        let host: String
        let port: UInt16
        let usermane: String?
        let password: String?
        let databaseName: String = "foodtruckapi"

        if let tempHost = credentials["host"] as? String, let tempUsername = credentials["username"] as? String,
            let tempPassword = credentials["password"] as? String, let tempPort = credentials["port"] as? Int {
            host = tempHost
            usermane = tempUsername
            password = tempPassword
            port = UInt16(tempPort)

            Log.info("Using CF Service Credentials")

        } else {
            host = "localhost"
            usermane = "iuli"
            password = "123456"
            port = UInt16(5984)
            Log.info("Using Service Deployment Credentials")
        }

        self.init(database: databaseName, host: host, port: port, usermane: usermane, password: password)
    }

    private func setupDb() {
        let couchClient = CouchDBClient(connectionProperties: connectionProps)
        Log.info("host: \(connectionProps.host)")
        Log.info("port: \(connectionProps.port)")
        Log.info("secured: \(connectionProps.secured)")

        couchClient.retrieveDB(dbName) { (foundDb, error) in
            if foundDb != nil {
                self.couchDB = foundDb
                Log.info("exists")
            } else {
                Log.error("DB does not exist: \(String(describing: error))")
                couchClient.createDB(self.dbName, callback: { (foundDb, error) in
                    if foundDb != nil {
                        self.couchDB = foundDb
                        Log.info("DB created!")
                        self.setupDbDesign()
                    } else {
                        Log.error("Unable to create DB: \(self.dbName).Error: \(String(describing: error))")
                        fatalError("Unable to create DB: \(self.dbName).Error: \(String(describing: error))")
                    }
                })
            }

        }

    }

    private func setupDbDesign() {
        guard let database = couchDB else {
            fatalError("No database")
        }
        let design = DesignDocument(_id: "_design/foodtruckdesign", views: [
            "all_documents": [
                "map": "function(doc) { emit(doc._id, [doc.id, doc._rev]); }"
            ],
            "all_trucks": [
                "map": "function(doc) { if(doc.type == 'foodtruck') {  emit(doc._id, [doc._id, doc.name, doc.foodtype, doc.avgcost, doc.latitude, doc.longitude]); } }"
            ],
            "total_trucks": [
                "map": "function(doc) { if(doc.type == 'foodtruck') { emit(doc._id, 1); } }",
                "reduce": "_count"
            ],
            "all_reviews": [
                "map": "function(doc) { if(doc.type == 'review') { emit(doc.truckid, [doc._id, doc.truckid, doc.reviewtitle, doc.reviewtext, doc.starrating]); } }"
            ],
            "total_reviews": [
                "map": "function(doc) { if(doc.type == 'review') { emit(doc.truckid, 1); } }",
                "reduce": "_count"
            ],
            "avg_rating": [
                "map": "function(doc) { if(doc.type == 'review') { emit(doc.truckid, doc.starrating); }}",
                "reduce": "_stats"
            ]
        ])
        database.createDesign(self.designName, document: design) { (_, error) in
            if error != nil {
                Log.error("Failed to create design \(String(describing: error))")
            } else {
                Log.info("Design created")
            }
        }
    }

    func getAllTrucks(completion: @escaping ([FoodTruck]?, Swift.Error?) -> Void) {
        foodtruckApi.getAllTrucks(completion: completion)
    }

    func addTruck(_ truck: FoodTruck, completion: @escaping (FoodTruck?, Swift.Error?) -> Void) {
        foodtruckApi.addTruck(truck, completion: completion)
    }

    func getTruck(docId: String, completion: @escaping (FoodTruck?, Swift.Error?) -> Void) {
        foodtruckApi.getTruck(docId: docId, completion: completion)
    }

    func updateTruck(docId: String, with newTruck: FoodTruck, completion: @escaping (FoodTruck?, Swift.Error?) -> Void) {
        foodtruckApi.updateTruck(docId: docId, with: newTruck, completion: completion)
    }

    func clearAll(completion: @escaping (Swift.Error?) -> Void) {
        foodtruckApi.clearAll(completion: completion)
    }

    func countTrucks(completion: @escaping (Int?, Swift.Error?) -> Void) {
        foodtruckApi.countTrucks(completion: completion)
    }

    func addReview(_ review: Review, completion: @escaping (Review?, Swift.Error?) -> Void) {
        reviewApi.addReview(review, completion: completion)
    }

    func getReviews(for truckId: String, completion: @escaping ([Review]?, Swift.Error?) -> Void) {
        reviewApi.getReviews(for: truckId, completion: completion)
    }

    func getReview(docId: String, completion: @escaping (Review?, Swift.Error?) -> Void) {
        reviewApi.getReview(docId: docId, completion: completion)
    }

    func updateReview(docId: String, with newReview: Review, completion: @escaping (Review?, Swift.Error?) -> Void) {
        reviewApi.updateReview(docId: docId, with: newReview, completion: completion)
    }

    func deleteReview(docId: String, completion: @escaping (Swift.Error?) -> Void) {
        reviewApi.deleteReview(docId: docId, completion: completion)
    }

    func deleteReviews(for truckId: String, completion: @escaping (Swift.Error?) -> Void) {
        reviewApi.deleteReviews(for: truckId, completion: completion)
    }

    func countReviews(for truckId: String?, completion: @escaping (Int?, Swift.Error?) -> Void) {
        reviewApi.countReviews(for: truckId, completion: completion)
    }

    func countReviews(completion: @escaping (Int?, Swift.Error?) -> Void) {
        reviewApi.countReviews(completion: completion)
    }

    func averageRating(for truckId: String, completion: @escaping (Int?, Swift.Error?) -> Void) {
        reviewApi.averageRating(for: truckId, completion: completion)
    }

    public func deleteTruck(docId: String, completion: @escaping (Swift.Error?) -> Void) {
        let dispatch = DispatchSemaphore(value: 1)

        deleteReviews(for: docId) { (err) in
            dispatch.signal()
            if let err = err {
                completion(err)
                return
            }
        }
        dispatch.wait()

        foodtruckApi.deleteTruck(docId: docId) { (err) in
            dispatch.signal()
            completion(err)
            return
        }

    }
}
