//
//  ReviewAPISrv.swift
//  Application
//
//  Created by iulian david on 26/10/2019.
//

import Foundation
import LoggerAPI
import CouchDB
import KituraNet

class ReviewAPISrv: ReviewAPI {

    let database: Database
    var requestOptions: [ClientRequest.Options]
    let designName = "foodtruckdesign"
    let dbName: String

    init(db: Database, requestOptions: [ClientRequest.Options], dbName: String) {
        self.database = db
        self.requestOptions = requestOptions
        self.dbName = dbName
    }

    func addReview(_ review: Review, completion: @escaping (Review?, Swift.Error?) -> Void) {
        guard review.truckId != nil else {
            return  completion(nil, APIControllerError.dbError(reason: "Invalid review"))
        }
        database.create(review) { (document, err) in
            if let docId = document?.id {
                self.getReview(docId: docId, completion: completion)
            } else {
                completion(nil, err)
            }
        }
    }

    func getReviews(for truckId: String, completion: @escaping ([Review]?, Swift.Error?) -> Void) {
        database.queryByView("all_reviews", ofDesign: designName, usingParameters: [.keys([truckId as ValueType]), .descending(true), .includeDocs(true)]) { (docs, error) in

            guard let documents = docs else {
                Log.error("Error retrieving all reviews for <truckID>: \(truckId) : \(String(describing: error))")
                return completion(nil, error)
            }

            let reviews = documents.decodeDocuments(ofType: Review.self)
            completion(reviews, nil)
        }
    }

    func getReview(docId: String, completion: @escaping (Review?, Swift.Error?) -> Void) {
        database.retrieve(docId, callback: { (response: Review?, err) in
            guard let review = response else {
                Log.error("Error retrieving <docID> \(docId) : \(String(describing: err))")
                return completion(nil, err)
            }

            completion(review, nil)
        })
    }

    /// Update an existing review. Most of the columns can be changed except _id and truckId
    func updateReview(docId: String, with newReview: Review, completion: @escaping (Review?, Swift.Error?) -> Void) {
        getReview(docId: docId) { response, err in
            guard let review = response, let rev = review._rev, let truckId = review.truckId else {
                Log.error("Error retrieving <docID> \(docId) : \(String(describing: err))")
                return completion(nil, err)
            }

            guard let updatedReview = Review(_id: review._id, truckId: truckId,
                                             reviewTitle: newReview.reviewTitle ?? review.reviewTitle,
                                             reviewText: newReview.reviewText ?? review.reviewText,
                                             starRating: newReview.starRating) else {
                                                Log.error("Error retrieving <docID> \(docId) : \(String(describing: err))")
                                                return completion(nil, APIControllerError.parseError)
            }

            self.database.update(docId, rev: rev, document: updatedReview, callback: { (_, error) in
                guard error == nil else {
                    Log.error("Error: \(error!) updating <document>: \(updatedReview))")
                    return completion(nil, error!)
                }
                completion(updatedReview, nil)
            })
        }
    }

    func deleteReview(docId: String, completion: @escaping (Swift.Error?) -> Void) {
        getReview(docId: docId) { response, err in
            guard let review = response, let rev = review._rev else {
                Log.error("Error retrieving <docID> \(docId) : \(String(describing: err))")
                return completion(err)
            }

            self.database.delete(docId, rev: rev, callback: { (err) in
                completion(err)
                return
            })
            completion(nil)
        }

    }

    func deleteReviews(for truckId: String, completion: @escaping (Swift.Error?) -> Void) {
        getReviews(for: truckId, completion: { (reviews, err) in
            guard let reviews = reviews, err == nil else {
                completion(err)
                return
            }

            // Step through each review in the array
            for review in reviews where review._id != nil && review._rev != nil {
                self.database.delete(review._id!, rev: review._rev!, callback: { (err) in
                    guard err == nil else {
                        completion(nil)
                        return
                    }
                })
            }
        })
    }

    func countReviews(completion: @escaping (Int?, Swift.Error?) -> Void) {
        countReviews(for: nil, completion: completion)
    }

    func countReviews(for truckId: String? = nil, completion: @escaping (Int?, Swift.Error?) -> Void) {
        if let truckId = truckId {

            DBUtils.executeQueryOnView(requestOptions: requestOptions, usingDbName: dbName,
                                       usingDesign: designName, for: "total_reviews", usingParameters: [.keys([truckId as ValueType])]) { (result, error) in
                                        if let doc = result, error == nil {
                                            if let count = (doc["rows"] as? [[String: Any]])?.first?["value"] as? Int {
                                                completion(count, nil)
                                            } else {
                                                completion(0, nil)
                                            }
                                        } else {
                                            completion(nil, error)
                                        }
            }

        } else {
            DBUtils.executeQueryOnView(requestOptions: requestOptions, usingDbName: dbName, usingDesign: designName, for: "total_reviews") { (result, error) in
                if let doc = result, error == nil {
                    if let count = (doc["rows"] as? [[String: Any]])?.first?["value"] as? Int {
                        completion(count, nil)
                    } else {
                        completion(0, nil)
                    }
                } else {
                    completion(nil, error)
                }
            }
        }
    }

    // Average star rating for a specific truck
    public func averageRating(for truckId: String, completion: @escaping (Int?, Swift.Error?) -> Void) {

        DBUtils.executeQueryOnView(requestOptions: requestOptions, usingDbName: dbName,
                                   usingDesign: designName, for: "avg_rating",
                                   usingParameters: [.keys([truckId as ValueType])]) { (result, error) in
                                    if let doc = result, error == nil {
                                        if let sum = ((doc["rows"] as? [[String: Any]])?.first?["value"] as? [String: Any])?["sum"] as? Float,
                                            let count = ((doc["rows"] as? [[String: Any]])?.first?["value"] as? [String: Any])?["count"] as? Float {
                                            let average = Int(round(sum/count))
                                            completion(average, nil)
                                        } else {
                                            completion(1, nil)
                                        }
                                    } else {
                                        completion(nil, error)
                                    }
        }

    }
}
