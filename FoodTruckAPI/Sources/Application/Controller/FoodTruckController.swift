//
//  FoodTruckController.swift
//  Application
//
//  Created by iulian david on 11/05/2019.
//

import Kitura
import LoggerAPI
import Foundation

public final class FoodTruckController {

    private let router: Router
    private let apiGateway: FoodTruckAPI & ReviewAPI
    public let trucksPath = "api/v1/trucks"
    public let reviewsPath = "api/v1/reviews"

    init(router: Router, backend: FoodTruckAPI & ReviewAPI) {
        self.router = router
        self.apiGateway = backend
        routeSetup()
    }

    public func routeSetup() {

        //router.all("/*", middleware: BodyParser())

        // MARK: - Food Truck Routes Handling
        // All trucks
        router.get(trucksPath, handler: getTrucksNew)

        // Add Truck
        router.post(trucksPath, handler: addTruck)

        // Count Trucks
        router.get("\(trucksPath)/count", handler: countTrucks)

        // Get Specific Truck
        router.get("\(trucksPath)/", handler: getTruckById)
        //
        // Delete Truck
        router.delete("\(trucksPath)/", handler: deleteTruckById)
        //
        // Update Truck
        router.put("\(trucksPath)/", handler: updateTruckById)

        // MARK: - Reviews Routes Handling
        // Get all reviews for a specific truck
        router.get("\(trucksPath)/reviews/", handler: getAllReviewsForTruck)

        // Get specific review
        router.get("\(reviewsPath)/", handler: getReviewById)

        // Add review
        //        router.post("\(reviewsPath)/:truckid", allowPartialMatch: false, middleware: BodyParser())
        router.post("\(reviewsPath)/:truckid", handler: addReviewForTruck)

        // update review
        //        router.put("\(reviewsPath)/:id", allowPartialMatch: false, middleware: BodyParser())
        router.put("\(reviewsPath)/:id", handler: updateReviewById)

        // delete review
        router.delete("\(reviewsPath)/:id", handler: deleteReviewById)

        // Reviews Count
        router.get("\(reviewsPath)/count", handler: getReviewsCount)

        // Reviews Count for a specific truck
        router.get("\(reviewsPath)/count/:truckid", handler: getReviewsCountForTruck)

        // Average star rating for truck
        router.get("\(reviewsPath)/rating/:truckid", handler: getReviewsRatingForTruck)

    }

    // MARK: - TRUCKS Endpoints
    // 2
    private func getTrucksNew(completion: @escaping ([FoodTruck]?, RequestError?) -> Void) {
        apiGateway.getAllTrucks { (trucks, err) in
            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(nil, .badRequest)
            }

            guard let trucks = trucks else {

                Log.error("Failed to get trucks")
                return completion(nil, .badRequest)
            }

            return completion(trucks, nil)
        }
    }

    private func addTruck(truckItem: FoodTruck, completion: @escaping (FoodTruck?, RequestError?) -> Void) {
        apiGateway.addTruck(truckItem) { (truck, err) in
            Log.debug("Adding truck")
            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(nil, .badRequest)
            }

            guard let truck = truck else {
                Log.error("Truck not found")
                return completion(nil, .internalServerError)
            }

            Log.info("\(truck.name ?? "") added to Vehicle list")
            return completion(truck, nil)

        }

    }

    private func getTruckById(truckId: String, completion: @escaping (FoodTruck?, RequestError?) -> Void) {

        apiGateway.getTruck(docId: truckId) { (truck, err) in
            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(nil, .badRequest)
            }

            if let truck = truck {
                completion(truck, nil)
            } else {
                Log.warning("Could not find a truck by that ID")
                return completion(nil, .notFound)
            }
        }

    }

    private func deleteTruckById(docId: String, completion: @escaping (RequestError?) -> Void) {

        apiGateway.deleteTruck(docId: docId) { (err) in
            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(.badRequest)
            }

            Log.info("\(docId) successfully deleted")
            return completion(.ok)
        }

    }

    private func updateTruckById(docId: String?, truckItem: FoodTruck?, completion: @escaping (FoodTruck?, RequestError?) -> Void) {
        guard let docId = docId else {
            Log.error("No ID supplied")
            return completion(nil, RequestError(.badRequest, body: ["error": "No ID supplied"]))

        }

        guard let truckItem = truckItem else {
            Log.error("No body found in request")
            return completion(nil, RequestError(.badRequest, body: ["error": "No body found in request"]))
        }

        apiGateway.updateTruck(docId: docId, with: truckItem) { (truck, err) in
            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(nil, RequestError(.badRequest, body: ["error": "Update unsuccesfull"]))
            }
            if let truck = truck {
                Log.info("\(docId) successfully updated")
                return completion(truck, nil)
            } else {
                Log.error("Invalid truck returned")
                return completion(nil, RequestError(.badRequest, body: ["error": "Invalid truck returned"]))
            }
        }
    }

    private func countTrucks(completion: @escaping (Countable?, RequestError?) -> Void) {
        apiGateway.countTrucks { (count, err) in
            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(nil, .badRequest)
            }

            guard let count = count else {
                Log.error("Failed to get count")
                return completion(nil, .internalServerError)
            }

            let json = Countable(count)
            return completion(json, nil)
        }
    }

    // MARK: - Reviews Endpoints
    /// Get all reviews for a specific truck
    private func getAllReviewsForTruck(truckId: String, completion: @escaping ([Review]?, RequestError?) -> Void) {
        apiGateway.getReviews(for: truckId, completion: { (reviews, err) in

            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(nil, .badRequest)
            }

            guard let reviews = reviews else {
                Log.error("Failed to get reviews for \(truckId)")
                return completion(nil, .internalServerError)
            }

            return completion(reviews, nil)

        })

    }

    /// Get specific review
    private func getReviewById(docId: String?, completion: @escaping (Review?, RequestError?) -> Void) {

        guard let docId = docId else {
            let errMsg = "No ID supplied"
            Log.error(errMsg)
            return completion(nil, RequestError(.badRequest, body: ["error": errMsg]))
        }

        apiGateway.getReview(docId: docId) { (review, err) in
            guard err == nil else {
                Log.error(err.debugDescription)
                return completion(nil, .badRequest)
            }

            if let review = review {
                return completion(review, nil)
            } else {
                Log.warning("Could not find any review for that ID ")
                return completion(nil, .notFound)
            }
        }

    }

    /// Add review for a truck
    private func addReviewForTruck(request: RouterRequest, response: RouterResponse, next: () -> Void) {

        // First we test if the truck id is supplied
        guard let truckId = request.parameters["truckid"] else {
            let errMsg = "No Truck ID supplied"
            response.status(.badRequest).send(json: ["error": errMsg])
            Log.warning(errMsg)
            return
        }

        guard var review = try? request.read(as: Review.self) else {
            let errMsg = "Invalid review"
            response.status(.unprocessableEntity).send(json: ["error": errMsg])
            Log.warning(errMsg)
            return
        }
        review.truckId = truckId
        apiGateway.addReview(review) { (review, err) in
            Log.debug("Adding <review>\(String(describing: review)) for \(truckId)")
            guard err == nil else {
                Log.error(err.debugDescription)
                try? response.status(.badRequest).end()
                return
            }

            guard let review = review else {
                Log.error("Failed to add review for \(truckId)")
                try? response.status(.internalServerError).end()
                return
            }

            Log.info("\(review) added for \(truckId)")
            try? response.status(.OK).send(review).end()
        }

    }

    // update review
    private func updateReviewById(request: RouterRequest, response: RouterResponse, next: () -> Void) {
        // First we test if the truck id is supplied
        guard let docId = request.parameters["id"] else {
            let errMsg = "No Review ID supplied"
            response.status(.badRequest).send(json: ["error": errMsg])
            Log.warning(errMsg)
            return
        }
        guard let review = try? request.read(as: Review.self) else {
            let errMsg = "Invalid review"
            response.status(.unprocessableEntity).send(json: ["error": errMsg])
            Log.warning(errMsg)
            return
        }
        apiGateway.updateReview(docId: docId, with: review) { (updatedReview, err) in
            do {
                guard err == nil else {
                    try response.status(.badRequest).end()
                    Log.error(err.debugDescription)
                    return
                }

                if let updatedReview = updatedReview {
                    try response.status(.OK).send(updatedReview).end()
                } else {
                    Log.error("Invalid review returned")
                    try response.status(.badRequest).end()
                }
            } catch {
                Log.error(error.localizedDescription)
            }
        }
    }

    // delete review
    private func deleteReviewById(request: RouterRequest, response: RouterResponse, next: () -> Void) {
        // First we test if the truck id is supplied
        guard let docId = request.parameters["id"] else {
            let errMsg = "No Review ID supplied"
            response.status(.badRequest).send(json: ["error": errMsg])
            Log.warning(errMsg)
            return
        }
        apiGateway.deleteReview(docId: docId) { (err) in
            do {
                guard err == nil else {
                    try response.status(.badRequest).end()
                    Log.error(err.debugDescription)
                    return
                }

                try response.status(.OK).end()
                Log.info("\(docId) successfully deleted")
            } catch {
                Log.error(error.localizedDescription)
            }
        }
    }

    // Get Count of all reviews
    private func getReviewsCount(request: RouterRequest, response: RouterResponse, next: () -> Void) {

        apiGateway.countReviews { (count, err) in
            do {
                guard err == nil else {
                    try response.status(.internalServerError).end()
                    Log.error(err.debugDescription)
                    return
                }

                guard let count = count else {
                    try response.status(.internalServerError).end()
                    Log.error("failed to get count")
                    return
                }
                try response.status(.OK).send(json: ["count": count]).end()

            } catch {
                Log.error(error.localizedDescription)
            }
        }

    }

    // Reviews Count for a specific truck
    private func getReviewsCountForTruck(request: RouterRequest, response: RouterResponse, next: () -> Void) {

        // First we test if the truck id is supplied
        guard let truckId = request.parameters["truckid"] else {
            let errMsg = "No Review ID supplied"
            response.status(.badRequest).send(json: ["error": errMsg])
            Log.warning(errMsg)
            return
        }

        apiGateway.countReviews(for: truckId) { (count, err) in
            do {
                guard err == nil else {
                    try response.status(.badRequest).end()
                    Log.error(err.debugDescription)
                    return
                }

                guard let count = count else {
                    try response.status(.internalServerError).end()
                    Log.error("failed to get count")
                    return
                }
                try response.status(.OK).send(json: ["count": count]).end()

            } catch {
                Log.error(error.localizedDescription)
            }
        }

    }

    // Average star rating for truck
    private func getReviewsRatingForTruck(request: RouterRequest, response: RouterResponse, next: () -> Void) {
        // First we test if the truck id is supplied
        guard let truckId = request.parameters["truckid"] else {
            let errMsg = "No Review ID supplied"
            response.status(.badRequest).send(json: ["error": errMsg])
            Log.warning(errMsg)
            return
        }

        apiGateway.averageRating(for: truckId) { (rating, err) in
            do {
                guard err == nil else {
                    try response.status(.badRequest).end()
                    Log.error(err.debugDescription)
                    return
                }

                guard let rating = rating else {
                    try response.status(.internalServerError).end()
                    Log.error("failed to get count")
                    return
                }
                try response.status(.OK).send(json: ["avgrating": rating]).end()

            } catch {
                Log.error(error.localizedDescription)
            }
        }

    }
}
