//
//  FoodTruckAPITests.swift
//  ApplicationTests
//
//  Created by iulian david on 27/05/2019.
//

import Foundation
import Kitura
import KituraNet
import XCTest
import HeliumLogger
import LoggerAPI
import CouchDB

@testable import Application

class FoodTruckAPITests: XCTestCase {

    static var allTests: [(String, (FoodTruckAPITests) -> () throws -> Void)] {

        return [
            ("testAddTruck", testAddAndGetTruck),
            ("testUpdateTruck", testUpdateTruck),
            ("testCountTrucks", testCountTrucks),
            ("testClearAll", testClearAll),
            ("testDeleteTruck", testDeleteTruck)
        ]
    }

    var foodtruckAPI: FoodTruckAPI!

    var apiGateway: APIGateway!

    override func setUp() {
        apiGateway = APIGateway()
        foodtruckAPI = apiGateway.foodtruckApi
        super.setUp()
    }

    // we clean up all the data
    override func tearDown() {
        guard  let trucks = foodtruckAPI else {
            return
        }

        trucks.clearAll { (err) in
            guard err == nil else {
                XCTFail("Could not delete all documents")
                return
            }
        }

    }

    // Add and Get specfic truck
    func testAddAndGetTruck() {

        let addExpectation = expectation(description: "Add truck item")
        let newTruck = FoodTruck(name: "testAdd", foodType: "testType", avgCost: 0, latitude: 0, longitude: 0)!
        //First add new truck
        foodtruckAPI.addTruck(newTruck) { (truck, err) in
            guard err == nil else {
                print(err!)
                XCTFail(err!.localizedDescription)
                return
            }

            if let addedTruck = truck, let docId = addedTruck._id {
                self.foodtruckAPI.getTruck(docId: docId, completion: { (returnedTruck, _) in
                    // Assert that the added truck equals the returned truck
                    XCTAssertEqual(truck, returnedTruck)
                    addExpectation.fulfill()
                })
            }
        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "addTruck Timeout")
        }
    }

    func testUpdateTruck() {

        let updateExpectation = expectation(description: "Update truck item")
        let newTruck = FoodTruck(name: "testUpdate", foodType: "testUpdate", avgCost: 0, latitude: 0, longitude: 0)!
        //First add new truck
        foodtruckAPI.addTruck(newTruck) { (addedTruck, err) in
            guard err == nil, let addedTruck = addedTruck, let docId = addedTruck._id  else {
                XCTFail("Failed to add truck")
                return
            }

            let newTruck = FoodTruck(_id: nil, name: "UpdatedTruck")!
            //Update the added truck
            self.foodtruckAPI.updateTruck(docId: docId, with: newTruck, completion: { (updatedTruck, err) in
                guard err == nil else {
                    XCTFail(err!.localizedDescription)
                    return
                }
                // Assert that the added truck equals the returned truck
                guard let updatedTruck = updatedTruck else {
                    XCTFail("failed to udpate")
                    return
                }

                //Fetch the truck from DB
                self.foodtruckAPI.getTruck(docId: docId, completion: { (fetchedTruck, err) in

                    guard err == nil else {
                        XCTFail(err!.localizedDescription)
                        return
                    }
                    // Assert that the fetched truck equals the updated truck
                    guard let fetchedTruck = fetchedTruck else {
                        XCTFail("unable to retrieve truck")
                        return
                    }

                    XCTAssertEqual(fetchedTruck.name, "UpdatedTruck")
                    XCTAssertEqual(fetchedTruck, updatedTruck)

                    updateExpectation.fulfill()
                })

            })

        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "updated Truck Timeout")
        }
    }

    func testCountTrucks() {

        let countExpectation = expectation(description: "Count trucks")
        //Add more trucks
        for _ in 0...25 {
            let newTruck = FoodTruck(name: "testCount", foodType: "testCount", avgCost: 0, latitude: 0, longitude: 0)!
            foodtruckAPI.addTruck(newTruck, completion: { (_, err) in
                guard err == nil else {
                    XCTFail("Could not add truck")
                    return
                }
            })
        }

        //Update the added truck
        foodtruckAPI.countTrucks { (count, err) in
            guard err == nil else {
                XCTFail(err!.localizedDescription)
                return
            }

            guard let count = count else {
                XCTFail("expected a result")
                return
            }
            // Assert value 26 is return
            XCTAssertEqual(count, 26)
            countExpectation.fulfill()
        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "Count Truck Timeout")
        }
    }

    func testClearAll() {

        let clearAllExpectation = expectation(description: "Clear all DB documents")
        let newTruck = FoodTruck(_id: nil, name: "testClearAll", foodType: "testClearAll", avgCost: 0, latitude: 0, longitude: 0)!
        foodtruckAPI.addTruck(newTruck) { (_, err) in

            guard err == nil else {
                XCTFail("Could not add truck")
                return
            }

            self.foodtruckAPI.clearAll { (_) in

            }

            self.foodtruckAPI.countTrucks { (count, _) in
                XCTAssertEqual(count, 0)

                //                trucks.countReviews(completion: { (count, _) in
                //                    XCTAssertEqual(count, 0)
                clearAllExpectation.fulfill()

                //                })

            }

        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "clearAll Timeout")
        }
    }

    func testDeleteTruck() {

        let deleteExpectation = expectation(description: "Delete a specific truck")

        let newTruck = FoodTruck(_id: nil, name: "deleteCount", foodType: "deleteCount", avgCost: 0, latitude: 0, longitude: 0)!
        //First add new truck
        foodtruckAPI.addTruck(newTruck) { (addedTruck, err) in
            guard err == nil else {
                XCTFail("Could not add truck")
                return
            }

            if let addedTruck = addedTruck, let docId = addedTruck._id {

                self.foodtruckAPI.deleteTruck(docId: docId, completion: { (err) in
                    guard err == nil else {
                        XCTFail("Could not delete truck")
                        return
                    }

                    // Count trucks to assert that count == 0
                    self.foodtruckAPI.countTrucks(completion: { (count, err) in
                        guard err == nil else {
                            XCTFail("Could not count trucks")
                            return
                        }

                        XCTAssertEqual(count, 0, "Delete truck not working")
                        deleteExpectation.fulfill()
                    })
                })

            } else {
                XCTFail("Could not add trucks")
            }
        }
        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "delete truck Timeout")
        }
    }
}
