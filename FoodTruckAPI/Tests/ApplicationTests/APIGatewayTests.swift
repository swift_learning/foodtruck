//
//  APIGatewayTests.swift
//  Application
//
//  Created by iulian david on 26/10/2019.
//

import Foundation
import Kitura
import KituraNet
import XCTest
import HeliumLogger
import LoggerAPI
import CouchDB

@testable import Application

class APIGatewayTests: XCTestCase {

    static var allTests: [(String, (APIGatewayTests) -> () throws -> Void)] {

        return [
            ("testDeleteTruck", testDeleteTruck)
        ]
    }

    var apiGateway: APIGateway!

    override func setUp() {
        apiGateway = APIGateway()
        super.setUp()
    }

    // we clean up all the data
    override func tearDown() {

        apiGateway.clearAll { (err) in
            guard err == nil else {
                XCTFail("Could not delete all documents")
                return
            }
        }

    }

 func testDeleteTruck() {

        let deleteExpectation = expectation(description: "Delete a specific truck")

        let newTruck = FoodTruck(_id: nil, name: "deleteCount", foodType: "deleteCount", avgCost: 0, latitude: 0, longitude: 0)!
        //First add new truck
        apiGateway.addTruck(newTruck) { (addedTruck, err) in
            guard err == nil else {
                XCTFail("Could not add truck")
                return
            }

            if let addedTruck = addedTruck, let docId = addedTruck._id {

                // Second add a review
                let review = Review(truckId: docId, reviewTitle: "testDelete", reviewText: "testDelete", starRating: 0)!
                // Just a small hack
                self.apiGateway.addReview(review) { (_, err) in
                    guard err == nil else {
                        XCTFail("Cannot add review")
                        return
                    }
                }

                // Third Delete the truck
                self.apiGateway.deleteTruck(docId: docId, completion: { (err) in
//                self.foodtruckAPI.deleteTruck(docId: docId, completion: { (err) in
                    guard err == nil else {
                        XCTFail("Could not delete truck")
                        return
                    }

                    // Count trucks to assert that count == 0
                    self.apiGateway.countTrucks(completion: { (count, err) in
                        guard err == nil else {
                            XCTFail("Could not count trucks")
                            return
                        }

                        XCTAssertEqual(count, 0, "Delete truck not working")
                        self.apiGateway.countReviews(for: docId, completion: { (countReviews, err) in
                            guard err == nil else {
                                XCTFail("Cannot count reviews")
                                return
                            }
                            XCTAssertEqual(countReviews, 0)
                            deleteExpectation.fulfill()
                        })
                    })
                })

            } else {
                XCTFail("Could not add trucks")
            }
        }
        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "delete truck Timeout")
        }
    }
}
