//
//  ReviewAPITests.swift
//  Application
//
//  Created by iulian david on 23/10/2019.
//

import XCTest
import Dispatch
@testable import Application

class ReviewAPITests: XCTestCase {

    static var allTests: [(String, (ReviewAPITests) -> () throws -> Void)] {

        return [

            ("testGetReviewsForTruck", testGetReviewsForTruck),
            ("testGetReviewById", testGetReviewById),
            ("testUpdateReview", testUpdateReview),
            ("testDeleteReview", testDeleteReview),
            ("testCountAllReviews", testCountAllReviews),
            ("testCountReviewsForTruck", testCountReviewsForTruck),
            ("testGetRating", testGetRating)
        ]
    }

    var reviewAPI: ReviewAPI!
    var apiGateway: APIGateway!

    override func setUp() {
        apiGateway = APIGateway()
        reviewAPI = apiGateway.reviewApi
        super.setUp()
    }

    // we clean up all the data
    override func tearDown() {
        apiGateway.clearAll { (err) in
            guard err == nil else {
                return
            }
        }

    }

    /// Helper method to add a food truck item and return the added item
    private func addTestTruck(dispatch: DispatchSemaphore, completion: @escaping (FoodTruck?, ReviewAPI?) -> Void) {
        /// Making the dispatch to wait forces the other to not execute
        dispatch.wait()

        let newtruck = FoodTruck(_id: nil, name: "testTruck", foodType: "testTruck", avgCost: 0, latitude: 0, longitude: 0)!
        apiGateway.addTruck(newtruck) { (addedTruck, err) in
            guard err == nil else {
                XCTFail("Failed to add truck")
                return
            }

            if let addedTruck = addedTruck, let docId = addedTruck._id {

                self.apiGateway.getTruck(docId: docId, completion: { (returnedTruck, _) in
                    // Assert that the added truck equals the returned truck
                    XCTAssertEqual(addedTruck, returnedTruck)

                    dispatch.signal()
                    completion(returnedTruck, self.apiGateway.reviewApi)

                })
            }
        }
    }

    func testGetReviewsForTruck() {
        // Forcing asyncrounous tasks synchronous
        let dispatch = DispatchSemaphore(value: 1)

        let reviewsForTruckExpectation = expectation(description: "Test reviews for specific truck")

        // Use the add truck method
        addTestTruck(dispatch: dispatch) { (addedTuck, reviewAPI) in
            guard let addedTruck = addedTuck, let truckId = addedTruck._id, let reviewAPI = reviewAPI else {
                XCTFail("Failed to add truck")
                return
            }

            let review1 = Review(truckId: truckId, reviewTitle: "testReview1", reviewText: "testReview1", starRating: 0)!
            reviewAPI.addReview(review1) { (review, err) in
                guard err == nil, review != nil else {
                    XCTFail("Failed to add review")
                    return
                }

                let review2 = Review(truckId: truckId, reviewTitle: "testReview2", reviewText: "testReview2", starRating: 0)!
                reviewAPI.addReview(review2) { (review, err) in
                    guard err == nil, review != nil else {
                        XCTFail("Failed to add review")
                        return
                    }

                    // Fetch the review
                    reviewAPI.getReviews(for: truckId, completion: { (reviews, err) in
                        guard err == nil else {
                            XCTFail("Cannot get all reviews for truck")
                            return
                        }
                        if let reviews = reviews {
                            XCTAssertEqual(reviews.count, 2)
                            XCTAssertEqual(reviews[0].truckId, truckId)
                            XCTAssertEqual(reviews[1].truckId, truckId)
                            reviewsForTruckExpectation.fulfill()
                        }
                    })
                }

            }

        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "addReview Timeout")
        }
    }

    func testGetReviewById() {

        let getReviewByIdExpectation = expectation(description: "Test Get review by ID")

        // Add a new review (Don't care if a truck actually exists here)
        let mockTruckId = "123456789"
        let review = Review(truckId: mockTruckId, reviewTitle: "test", reviewText: "test", starRating: 0)!
        reviewAPI.addReview(review) { (addedReview, err) in
            guard err == nil, let reviewId = addedReview?._id else {
                XCTFail("Failed to add review")
                return
            }

            self.reviewAPI.getReview(docId: reviewId, completion: { (fetchedReview, err) in
                guard err == nil else {
                    XCTFail("Cannot retrieve review for <_id>: \(reviewId)")
                    return
                }
                XCTAssertEqual(fetchedReview, addedReview)
                getReviewByIdExpectation.fulfill()
            })
        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "addReview Timeout")
        }
    }

    func testUpdateReview() {

        let updateReviewExpectation = expectation(description: "Test Update Review")

        // Add a new review (Don't care if a truck actually exists here)
        let mockTruckId = "123456789"
        let review = Review(truckId: mockTruckId, reviewTitle: "test", reviewText: "test", starRating: 0)!
        reviewAPI.addReview(review) { (addedReview, err) in
            guard err == nil, let reviewId = addedReview?._id else {
                XCTFail("Failed to add review")
                return
            }

            let newReview = Review(truckId: "1", reviewTitle: "update", reviewText: "update", starRating: 0)!
            self.reviewAPI.updateReview(docId: reviewId, with: newReview, completion: { (updatedReview, err) in
                guard err == nil, let reviewId = updatedReview?._id else {
                    XCTFail("Failed to update")
                    return
                }

                self.reviewAPI.getReview(docId: reviewId, completion: { (fetchedReview, err) in
                    guard err == nil else {
                        XCTFail("Failed to get review")
                        return
                    }
                    XCTAssertEqual(fetchedReview, updatedReview)
                    XCTAssertEqual(fetchedReview?.reviewTitle, "update")
                    updateReviewExpectation.fulfill()
                })

            })

        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "updateReview Timeout")
        }
    }

    func testDeleteReview() {

        let deleteReviewExpectation = expectation(description: "Test Delete Review")

        // Add a new review (Don't care if a truck actually exists here)
        let mockTruckId = "123456789"
        let review = Review(truckId: mockTruckId, reviewTitle: "testDelete", reviewText: "testDelete", starRating: 0)!
        reviewAPI.addReview(review) { (addedReview, err) in
            guard err == nil, let reviewId = addedReview?._id else {
                XCTFail("Failed to add review")
                return
            }

            self.reviewAPI.deleteReview(docId: reviewId, completion: { (err) in
                guard err == nil else {
                    XCTFail("Fail to delete review")
                    return
                }
            })

            self.reviewAPI.countReviews(completion: { (count, err) in
                guard err == nil else {
                    XCTFail("Failed to count reviews")
                    return
                }

                XCTAssertEqual(count, 0)
                deleteReviewExpectation.fulfill()
            })

        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "deleteReview Timeout")
        }
    }

    func testCountAllReviews() {

        let countAllReviewsExpectation = expectation(description: "Test Count ALL Reviews")

        // Add 2 reviews and then count them (fictious trucks)
        let review1 = Review(truckId: "truck1", reviewTitle: "review1", reviewText: "text1", starRating: 0)!
        reviewAPI.addReview(review1) { (_, err) in
            guard err == nil else {
                XCTFail("Failed to add review\(review1)")
                return
            }

            let review2 = Review(truckId: "truck2", reviewTitle: "review2", reviewText: "text2", starRating: 0)!
            self.reviewAPI.addReview(review2, completion: { (_, err) in
                guard err == nil else {
                    XCTFail("Failed to add review\(review2)")
                    return
                }

                self.reviewAPI.countReviews(completion: { (count, err) in
                    guard err == nil else {
                        XCTFail("Failed to count review")
                        return
                    }

                    if let count = count {
                        XCTAssertEqual(count, 2)
                        countAllReviewsExpectation.fulfill()
                    }
                })
            })
        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "countReviews Timeout")
        }
    }

    func testCountReviewsForTruck() {

        let countReviewForTruckExpectation = expectation(description: "Test Count ALL Reviews For truck")

        // Add 26 reviews and then count them (fictious trucks)
        for index in 0...25 {
            let review = Review(truckId: "truck1", reviewTitle: "review\(index)", reviewText: "text\(index)", starRating: 0)!
            reviewAPI.addReview(review) { (_, err) in
                guard err == nil else {
                    XCTFail("Failed to add review \(review)")
                    return
                }

            }
        }

        reviewAPI.countReviews(for: "truck1", completion: { (count, err) in
            guard err == nil else {
                XCTFail("Failed to count reviews")
                return
            }

            XCTAssertEqual(count, 26)
            countReviewForTruckExpectation.fulfill()
        })

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "countReviews Timeout")
        }
    }

    func testGetRating() {

        let getAvgRatingForTruckExpectation = expectation(description: "Test Get Average Rating For Truck")

        // Add 26 reviews and then count them (fictious trucks)
        for index in 0...25 {
            let review = Review(truckId: "truck1", reviewTitle: "review\(index)", reviewText: "text\(index)", starRating: index)!
            reviewAPI.addReview(review) { (_, err) in
                guard err == nil else {
                     XCTFail("Failed to add review \(review)")
                    return
                }

            }
        }

        reviewAPI.averageRating(for: "truck1") { (rating, err) in
            guard err == nil else {
                XCTFail("Failed to retrieve rating")
                return
            }
            XCTAssertNotEqual(rating, 0)
            XCTAssertEqual(rating!, 13)
            getAvgRatingForTruckExpectation.fulfill()
        }

        waitForExpectations(timeout: 5) { (err) in
            XCTAssertNil(err, "averageRating Timeout")
        }

    }
}
