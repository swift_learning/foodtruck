import XCTest

@testable import ApplicationTests

XCTMain([
    testCase(RouteTests.allTests),
    testCase(FoodTruckAPITests.allTests),
    testCase(ReviewAPITests.allTests),
    testCase(APIGatewayTests.allTests)
    ])
