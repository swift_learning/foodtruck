//
//  AddTuckVC.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/12/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

let bShowMapWithSpan = false

let iphoneSEWidth: CGFloat = 320
let iphoneSEHeight: CGFloat = 568
class AddTuckVC: UIViewController {

    var dataService = DataService.instance
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var foodTypeField: UITextField!
    @IBOutlet weak var avgCostField: UITextField!
    @IBOutlet weak var addressText: UICustomTextView!
    @IBOutlet weak var latField: UITextField!
    @IBOutlet weak var lonField: UITextField!

    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var coordinateView: UIView!
    @IBOutlet weak var actionsView: UIView!

    @IBOutlet weak var spinner: UIActivityIndicatorView!

    @IBOutlet weak var mapView: MKMapView!

    lazy var geocoder = CLGeocoder()

    override func viewDidLoad() {
        super.viewDidLoad()
        latField.delegate = self
        lonField.delegate = self
        addressText.delegate = self

        // dismissKeyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

        setupUISizes()

        //setup mapKit to get a place on long press gesture
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(revealRegionDetailsWithLongPressOnMap(sender:)))
        lpgr.minimumPressDuration = 2
        mapView.addGestureRecognizer(lpgr)
    }

    fileprivate func setupUISizes() {
        //animations
        let addressViewHeight = round(addressView.bounds.height)
        let width = view.bounds.width
        addressView.frame.size = CGSize(width: width, height: addressViewHeight)
        addressView.frame.origin.x = -width
        coordinateView.frame.size = CGSize(width: width, height: addressViewHeight)
        coordinateView.frame.origin.x = width

        self.view.layoutIfNeeded()

        UIView.animate(withDuration: 0.3, animations: {
            self.addressView.transform = CGAffineTransform(translationX: self.view.bounds.width, y: 0)
        })
    }

    fileprivate func animateCoordinatesView() {
        return UIView.animate(withDuration: 0.3, animations: {
            self.addressView.transform = .identity
            self.coordinateView.transform = CGAffineTransform(translationX: -self.view.bounds.width, y: 0)
        })
    }

    @IBAction func showCoordinatesView(_ sender: Any) {
        animateCoordinatesView()
    }
    @IBAction func showAddressView(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.addressView.transform = CGAffineTransform(translationX: self.view.bounds.width, y: 0)
            self.coordinateView.transform = .identity
        })

    }

    @IBAction func backButtonTapped(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    //hide the keyboard
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    @IBAction func addButtonTapped(sender: UIButton) {

        spinner.startAnimating()
        guard let truckName = self.nameField.text, self.nameField.text != "" else {
            self.showAlert(with: "Error", message: "Please Enter a name")
            return
        }

        guard let foodType = self.foodTypeField.text, self.foodTypeField.text != "" else {
            self.showAlert(with: "Error", message: "Please enter a food type")
            return
        }

        guard let avgCost = Double(self.avgCostField.text!), avgCost > 0 else {
            self.showAlert(with: "Error", message: "Please enter an average Cost")
            return
        }
        guard let latitude = Double(self.latField.text!), latitude >= -90, latitude <= 90 else {
            self.showAlert(with: "Error", message: "Please enter a valid latitude, between -90 and 90 ")
            return
        }
        guard let longitude = Double(self.lonField.text!), longitude >= -180, longitude <= 180 else {
            self.showAlert(with: "Error", message: "Please enter a valid longitude, between -180 and 180")
            return
        }

        dataService.addNewFoodTruck(truckName, foodType: foodType, avgcost: avgCost, latitude: latitude, longitude: longitude, completion: { (success) in
            if success {
                print("Saved successfully")
                self.dismiss(animated: true, completion: nil)

//                self.dataService.getAllFoodTrucks(completion: { (success) in
//                    if success {
//                        self.dismiss(animated: true, completion: nil)
//                    } else {
//                        print("An error occurred")
//                        self.showAlert(with: "Error", message: "An unknown error occurred")
//                        
//                    }
//                })

            } else {
                self.showAlert(with: "Error", message: "An error occurred saving the new food truck")
                print("Failed to save")

            }
        })
    }

    // Get touch point store it intno textFields and force map sto recenter and show annotation
    @objc func revealRegionDetailsWithLongPressOnMap(sender: UILongPressGestureRecognizer) {

        guard sender.state != UIGestureRecognizer.State.began else { return }

        let touchPoint = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        latField.text = String(locationCoordinate.latitude)
        lonField.text = String(locationCoordinate.longitude)

        showMap(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
    }

    @IBAction func cancelButtonTapped(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        OperationQueue.main.addOperation {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

    func showAlert(with title: String?, message: String?) {
        let alertController  = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)

        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        spinner.stopAnimating()
    }

    fileprivate func showMap(latitude: Double, longitude: Double) {
        mapView.isZoomEnabled = true
        mapView.isHidden = false
        let centerCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)

        if bShowMapWithSpan {
            let span = MKCoordinateSpan(latitudeDelta: 0.035, longitudeDelta: 0.035)
            let region = MKCoordinateRegion(center: centerCoordinate, span: span)
            self.mapView.setRegion(region, animated: true)
        } else {
            let coordinateRegion = MKCoordinateRegion(center: centerCoordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            self.mapView.setRegion(coordinateRegion, animated: true)
        }

        let annotation = MKPointAnnotation()
        annotation.coordinate = centerCoordinate

        annotation.title = (nameField.text?.count)! > 0 ? nameField.text! : "You set it here"
        self.mapView.addAnnotation(annotation)
        // Too much of zoom
        //        let aPoint:MKMapPoint = MKMapPointForCoordinate(annotation.coordinate)
        //        let rect:MKMapRect = MKMapRectMake(aPoint.x, aPoint.y, 0.5, 0.5)
        //        self.mapView.setVisibleMapRect(rect, animated: true)

    }

}

extension AddTuckVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let latitude = Double(self.latField.text!), let longitude = Double(self.lonField.text!) else {
            return
        }

        UIView.animate(withDuration: 0.3) {
            self.showMap(latitude: latitude, longitude: longitude)
        }
        textField.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

extension AddTuckVC: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        self.getCoordinatesFromAddress(address: textView.text)
    }

    fileprivate func addressToCoordinates(_ address: String) {
        geocoder.geocodeAddressString(address) { [unowned self] (placeMarks, _) -> Void in

            let placeMark = placeMarks?.first

            let coordinates = placeMark?.location?.coordinate

            DispatchQueue.main.async(execute: {
                guard let latitude = coordinates?.latitude, let longitude = coordinates?.longitude else {
                    return
                }
                self.latField.text = String(latitude)
                self.lonField.text = String(longitude)
                self.showMap(latitude: latitude, longitude: longitude)
            })
        }
    }

    fileprivate func getCoordinatesFromAddress(address inputAddress: String?) {
        guard let address = inputAddress, address.count > 0 else {
            return
        }
        addressToCoordinates(address)
    }
}
