//
//  AddreviewVC.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/12/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class AddReviewVC: UIViewController {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var reviewTitleTextField: UITextField!
    @IBOutlet weak var reviewTextTextView: UITextView!
    @IBOutlet weak var starRatingStepper: UIStepper!
    @IBOutlet weak var starRatingLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var dataService = DataService.instance

    var selectedFoodTruck: FoodTruck?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard  let truck = selectedFoodTruck else {
            dismissViewController()
            return
        }

        headerLabel.text = truck.name

        starRatingLabel.text = "Star Rating: \(Int(starRatingStepper.value))"

        // dismissKeyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }

    //hide the keyboard
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    func showAlert(with title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: title, preferredStyle: .alert)

        let action = UIAlertAction(title: "OK", style: .default, handler: nil)

        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
        self.spinner.stopAnimating()
    }

    @IBAction func addButtonTapped(sender: UIButton) {

        spinner.startAnimating()

        guard  let truck = selectedFoodTruck else {
            dismissViewController()
            return
        }

        guard let title = self.reviewTitleTextField.text, title != "" else {
            showAlert(with: "Error", message: "Please enter a title for your review")
            return
        }

        guard let text = self.reviewTextTextView.text, text != "" else {
            showAlert(with: "Error", message: "Please enter some text for your review")
            return
        }

        let rating = Int(starRatingStepper.value)

        dataService.addNewReview(truck.docId, title: title, text: text, rating: rating) { (success) in
            if success {
                print("We saved successfully")

                self.dataService.getAverageStarRatingForTruck(truck, completion: { (success) in
                    if success {
                        self.dismissViewController()
                    } else {
                        print("An error occurred")
                        self.showAlert(with: "Error", message: "An unknown error occurred")

                    }
                })

            } else {
                self.showAlert(with: "Error", message: "An error occurred saving the new review")
                print("An error occurred on saving the review")
            }
        }
    }

    @IBAction func stepperValueChanged(sender: UIStepper) {
        let newValue = Int(sender.value)
        starRatingLabel.text = "Star Rating: \(newValue)"
    }

    @IBAction func cancelButtonTapped(sender: UIButton) {
        dismissViewController()
    }

    @IBAction func backButtonTapped(sender: UIButton) {
        dismissViewController()
    }

    func dismissViewController() {
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

}
