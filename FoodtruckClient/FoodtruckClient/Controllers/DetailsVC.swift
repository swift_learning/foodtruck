//
//  DetailsVC.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/11/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import MapKit

class DetailsVC: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var foodTypeLabel: UILabel!
    @IBOutlet weak var avgCostLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var avgRatingLabel: UILabel!

    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var selectedFoodTruck: FoodTruck?
    var dataService = DataService.instance
    var avgRating: Int = -1

    override func viewWillAppear(_ animated: Bool) {
        dataService.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // If we don't have a selected food truck we go back
        guard let truck = selectedFoodTruck else {
            _ = navigationController?.popViewController(animated: true)
            return
        }

        spinner.startAnimating()
        dataService.getAverageStarRatingForTruck(truck) {_ in }

        nameLabel.text = truck.name
        foodTypeLabel.text = truck.foodType
        avgRatingLabel.text = "\(avgRating)"
        avgCostLabel.text = "$\(truck.avgCost)"

        // Adding the pin on the map
        mapView.addAnnotation(truck)
        centerMapOnLocation(CLLocation(latitude: truck.latitude, longitude: truck.longitude))

    }

    func centerMapOnLocation(_ location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: selectedFoodTruck!.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)

        mapView.setRegion(coordinateRegion, animated: true)
    }

    @IBAction func backButtonTapped(sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func reviewsButtonTapped(sender: UIButton) {
        performSegue(withIdentifier: "showReviewsVC", sender: self)
    }

    @IBAction func addReviewButtonTapped(sender: UIButton) {
        performSegue(withIdentifier: "showAddReviewVC", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReviewsVC" {

            if let destination = segue.destination as? ReviewsVC {
                destination.selectedFoodTruck = selectedFoodTruck
            }

        } else if segue.identifier == "showAddReviewVC" {
            if let destination = segue.destination as? AddReviewVC {
                destination.selectedFoodTruck = selectedFoodTruck
            }
        }
    }
}

extension DetailsVC: RatingServiceDelegate {

    func avgRatingUpdated(rating: Int) {
        DispatchQueue.main.async {
            self.avgRatingLabel.text = "\(rating)"
            self.spinner.stopAnimating()
        }
    }
}
