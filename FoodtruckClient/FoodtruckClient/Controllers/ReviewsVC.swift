//
//  ReviewsVC.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/12/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class ReviewsVC: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spiner: UIActivityIndicatorView!

    /// Lazy refresh control
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.black
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)

        return refreshControl
    }()

    var selectedFoodTruck: FoodTruck?
    var dataService = DataService.instance
    var reviews = [FoodTruckReview]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // must be added to tableView
        tableView.addSubview(self.refreshControl)

        tableView.dataSource = self
        tableView.delegate = self

        dataService.delegate = self

        // If we don't have a selected food truck we go back
        guard let truck = selectedFoodTruck else {
            _ = navigationController?.popViewController(animated: true)
            return
        }

        spiner.startAnimating()
        nameLabel.text = selectedFoodTruck?.name
        dataService.getAllReviews(truck) { _ in

        }

        // Setting auto resizing cells
        // For this to be done we need to set in the storyboard design
        // the the ** Lines ** property to 0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 140
    }

    @IBAction func backButtonTapped(sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

        dataService.getAllReviews(self.selectedFoodTruck!) { _ in }
        self.tableView.reloadData()
        self.spiner.stopAnimating()
        refreshControl.endRefreshing()
    }

}

extension ReviewsVC: UITableViewDelegate, UITableViewDataSource {

    // Setting auto resizing cells
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as? ReviewCell {
            cell.configureCell(review: reviews[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }

    }
}

extension ReviewsVC: ReviewServiceDelegate {

    func reviewLoaded(reviews: [FoodTruckReview]) {
        DispatchQueue.main.async {
            // Grab the main UI thread
            DispatchQueue.main.async {
                print("reviews loaded()")
                self.reviews = reviews
                self.tableView.reloadData()
                self.spiner.stopAnimating()
            }

        }
    }
}
