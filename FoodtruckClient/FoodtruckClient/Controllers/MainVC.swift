//
//  MainVC.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/10/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    /// Lazy refresh control
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.black
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)

        return refreshControl
    }()

    var dataService = DataService.instance
    var foodTrucks = [FoodTruck]()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataService.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        dataService.delegate = self

        // must be added to tableView
        tableView.addSubview(self.refreshControl)

        tableView.delegate = self
        tableView.dataSource = self

        spinner.startAnimating()

        dataService.getAllFoodTrucks {_ in }
    }

    @IBAction func addButtonTapped(sender: UIButton) {
        performSegue(withIdentifier: "showAddTruckVC", sender: self)
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        dataService.getAllFoodTrucks { _ in }
        self.tableView.reloadData()
        self.spinner.stopAnimating()
        refreshControl.endRefreshing()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailsVC" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                if let destination = segue.destination as? DetailsVC {
                    destination.selectedFoodTruck = self.foodTrucks[indexPath.row]
                }
            }
        }
    }

}

extension MainVC: FoodTrucksServiceDelegate {
    func trucksLoaded(trucks: [FoodTruck]) {
        // Grab the main UI thread
        DispatchQueue.main.async {
            self.foodTrucks = trucks
            print("trucks loaded()")
            self.tableView.reloadData()
            self.spinner.stopAnimating()
        }
    }
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodTrucks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FoodTruckCell", for: indexPath) as? FoodTruckCell {
            cell.configureCell(truck: foodTrucks[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }

    }
}
