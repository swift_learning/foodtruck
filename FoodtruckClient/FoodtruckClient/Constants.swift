//
//  Constants.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/10/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation

// Callbacks
/// Typealias for callbacks used in Data Service
typealias Callback = (_ success: Bool) -> Void

/// Base URL
let baseAPIUrl = Environment.rootURL

/// GET all food trucks
let getAllTrucksURL = "\(baseAPIUrl)/trucks"

/// GET all reviews for a specific food truck
let getAllReviewsURL = "\(baseAPIUrl)/trucks/reviews"

/// GET a star rating for a specific food truck
let getRatingURL = "\(baseAPIUrl)/reviews/rating"

/// POST a new Food Truck
let postAddTruckURL = "\(baseAPIUrl)/trucks"

/// POST add review for a specific truck
let postAddReviewURL = "\(baseAPIUrl)/reviews"
