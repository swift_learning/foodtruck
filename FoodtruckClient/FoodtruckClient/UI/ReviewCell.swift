//
//  ReviewCell.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/12/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reviewTextLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(review: FoodTruckReview) {
        titleLabel.text = review.reviewTitle
        reviewTextLabel.text = review.reviewText
    }

}
