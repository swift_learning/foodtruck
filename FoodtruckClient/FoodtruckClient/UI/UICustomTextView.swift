//
//  UICustomTextView.swift
//  FoodTruckClient
//
//  Created by iulian david on 7/13/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

@IBDesignable
class UICustomTextView: UITextView {

    @IBInspectable var dismissKeyboardWithToolBar: Bool = false {
        didSet {
            updateUI()
        }
    }

    func updateUI() {
        if dismissKeyboardWithToolBar {
            if let parentView = self.superview {
            //init toolbar
            let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: parentView.frame.size.width, height: 30))
            //create left side empty space so that done button set on right side
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dismissKeyboardAction))
            toolbar.setItems([flexSpace, doneBtn], animated: false)
            toolbar.sizeToFit()
            //setting toolbar as inputAccessoryView
            inputAccessoryView = toolbar
            }
        }
    }

    @objc func dismissKeyboardAction() {
        self.superview?.endEditing(true)
    }
}
