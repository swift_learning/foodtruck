//
//  UICustomButton.swift
//  FoodTruckClient
//
//  Created by iulian david on 7/13/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

@IBDesignable
class UICustomButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            updateUI()
        }
    }

    func updateUI() {
       layer.cornerRadius = cornerRadius
       clipsToBounds = true
    }

}
