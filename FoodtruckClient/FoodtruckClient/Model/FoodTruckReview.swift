//
//  FoodTruckReview.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/10/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation

struct FoodTruckReview {

    var docId: String = ""
    var truckId: String = ""
    var reviewTitle: String = ""
    var reviewText: String = ""
    var starRating: Int = 0

    static func parseReviewJSONData(data: Data) -> [FoodTruckReview] {

        var foodTruckReviews = [FoodTruckReview]()

        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)

            //Parse JSON data
            if let reviews = jsonResult as? [[String: AnyObject]] {
                for review in reviews {
                    guard let docId = review["_id"] as? String, docId != "",
                        let reviewTitle = review["reviewtitle"] as? String,
                        let reviewText = review["reviewtext"] as? String,
                        let truckId = review["truckid"] as? String,
                        let starRating = review["starrating"] as? Int else {
                            continue
                    }
                    let review = FoodTruckReview(docId: docId, truckId: truckId, reviewTitle: reviewTitle, reviewText: reviewText, starRating: starRating)
                    foodTruckReviews.append(review)
                }
            }
        } catch let err {
            print(err)
        }
        return foodTruckReviews
    }

}
