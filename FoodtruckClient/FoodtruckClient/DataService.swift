//
//  DataService.swift
//  FoodTruckClient
//
//  Created by iulian david on 4/10/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

/// Will be used for notification when some events are triggered
protocol MyServiceDelegate: class {
}
protocol FoodTrucksServiceDelegate: MyServiceDelegate {
    func trucksLoaded(trucks: [FoodTruck])
}

protocol ReviewServiceDelegate: MyServiceDelegate {
    func reviewLoaded(reviews: [FoodTruckReview])
}

protocol RatingServiceDelegate: MyServiceDelegate {
    func avgRatingUpdated(rating: Int)
}

class DataService {
    static let instance = DataService()

    weak var delegate: MyServiceDelegate?

//    // GET all trucks ** Alamofire and SwiftyJSON
//    func getAllFoodTrucks(completion: @escaping callback) {
//        let url = GET_ALL_FT_URL
//        
//        Alamofire.request(url, method: .get)
//            .validate(statusCode: 200..<300)
//            .responseData { (response) in
//                guard response.result.error == nil else {
//                    print("Alamofire request failed: \(String(describing: response.result.error))")
//                    completion(false)
//                    return
//                }
//                guard let data = response.data, let statusCode = response.response?.statusCode else {
//                    print("An error occured obtaining data")
//                    completion(false)
//                    return
//                }
//                
//                print("Alamofire request succeeded: HTTP \(statusCode)")
//                self.foodTrucks = FoodTruck.parseFoodTruckJSONData(data: data)
//                self.delegate?.trucksLoaded()
//                completion(true)
//        }
//    }

    // Get all trucks ** UrlSession
    func getAllFoodTrucks(completion: @escaping Callback) {

        let sessionConfig = URLSessionConfiguration.default

        // Create session, an optional set a URLSessionDelegate
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

        // Create request
        // Get all foodtrucks(GET /api/v1/trucks)
        guard let URL = URL(string: getAllTrucksURL) else { return }
        let request = URLRequest(url: URL)

        let task = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Request failed: \(String(describing: error.debugDescription))")
                completion(false)
                return
            }
            guard let data = data, let statusCode = (response as? HTTPURLResponse)?.statusCode, 200..<300 ~= statusCode else {
                completion(false)
                return
            }
            let foodTrucks = FoodTruck.parseFoodTruckJSONData(data: data)
            if let truckDelegate = self.delegate as? FoodTrucksServiceDelegate {
                truckDelegate.trucksLoaded(trucks: foodTrucks)
            }
            completion(true)
        }
        task.resume()
        session.finishTasksAndInvalidate()
    }

    // GET all reviews for a specific food truck
    func getAllReviews(_ truck: FoodTruck, completion: @escaping Callback) {
        Alamofire.request("\(getAllReviewsURL)/\(truck.docId)", method: .get)
        .validate(statusCode: 200..<300)
        .responseJSON { (response) in
            if response.result.error == nil {
                if let data = response.data {
                    let reviews = FoodTruckReview.parseReviewJSONData(data: data)
                    if let reviewDelegate = self.delegate as? ReviewServiceDelegate {
                        reviewDelegate.reviewLoaded(reviews: reviews)
                    }
                    completion(true)
                }
            } else {
                debugPrint("Alamofire request failed: \(String(describing: response.result.error))")
                completion(false)
            }
        }
    }

    // POST add a new truck
    func addNewFoodTruck(_ name: String, foodType: String, avgcost: Double, latitude: Double, longitude: Double, completion: @escaping Callback) {

        let url = postAddTruckURL

        // Add headers
        let headers = [
         "Content-Type": "application/json; charset=utf-8"
        ]

        // Construct json body
        let body: [String: Any] = [
            "name": name,
            "foodtype": foodType,
            "avgcost": avgcost,
            "latitude": latitude,
            "longitude": longitude
        ]

        // Request
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<299)
            .responseJSON { response in
                if response.result.error == nil {
                    guard response.response?.statusCode != nil else {
                        print("An error occurred")
                        completion(false)
                        return
                    }
                    self.getAllFoodTrucks { _ in
                        completion(true)
                    }

                } else {
                    debugPrint("Alamofire request failed: \(String(describing: response.result.error.debugDescription))")
                }
        }
    }

    // POST add a new truck review
    func addNewReview(_ foodTruckId: String, title: String, text: String, rating: Int, completion: @escaping Callback) {

        let url = "\(postAddReviewURL)/\(foodTruckId)"

        // Add headers
        let headers = [
            "Content-Type": "application/json; charset=utf-8"
            ]

        // Construct json body
        let body: [String: Any] = [
            "truckid": foodTruckId,
            "reviewtitle": title,
            "reviewtext": text,
            "starrating": rating
        ]

        // Request
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<299)
            .responseJSON { response in
                if response.result.error == nil {
                    guard response.response?.statusCode != nil else {
                        print("An error occurred")
                        completion(false)
                        return
                    }
                    completion(true)
                } else {
                    debugPrint("Alamofire request failed: \(String(describing: response.result.error.debugDescription))")
                }
        }
    }

    // GET avg star rating for a specific truck
    func getAverageStarRatingForTruck(_ truck: FoodTruck, completion: @escaping Callback) {
        let url = "\(getRatingURL)/\(truck.docId)"

        Alamofire.request(url, method: .get)
        .validate(statusCode: 200...299)
        .responseJSON { response in
            if response.result.error == nil {
                guard let value = response.result.value else {
                    completion(false)
                    return
                }
                let json = JSON(value)
                if let avgRating = json["avgrating"].int {
                    if let ratingDelegate = self.delegate as? RatingServiceDelegate {
                        ratingDelegate.avgRatingUpdated(rating: avgRating)
                    }
                }
                completion(true)
            } else {
                if let ratingDelegate = self.delegate as? RatingServiceDelegate {
                    ratingDelegate.avgRatingUpdated(rating: 0)
                }
                completion(false)
            }
        }
    }
}
