# README #

A full swift solution WEB API + UI based on Udemy Kitura Tutorial

### What is this repository for? ###



### How do I get set up? ###

* Summary of set up
* Dependencies
* Database configuration

CouchDB docker setup

Pull the couchdb Docker image

```sh
$docker pull couchdb
```

Then run it with the credentials stored in *config/cloundant_credentials.json*

```sh
$docker run --rm --name couchdb -p 5984:5984 -e COUCHDB_USER=iuli -e COUCHDB_PASSWORD=123456 couchdb
```
* Running tests

Tests can be run on docker using the docker-test.yml file (using --abort-on-container-exit to stop the couchdb instnce raise)
```sh
$ docker-compose -f docker-test.yml up --abort-on-container-exit
```

or Integration Tests via POSTMAN **it uses 8090 as port **
```sh
$ docker-compose up
```

* Deployment instructions
* Client configuration:

```sh
$ pod install
```

Launch project into Xcode:

```sh
open FoodTruckClient.xcworkspace
```

Generate FoodTruckAPI xcodeproj
![SwiftPM](screens/swiftPm.png)
configure SwiftLint ![WatchYourLanguage!](screens/watchYourLanguage.png)

From Xcode add files to project and select FoodTruckAPI.xcodeproj ![addproj.png](https://bitbucket.org/repo/gkbLqL4/images/3650738606-Screen%20Shot%202017-04-10%20at%206.18.25%20AM.png)


### Bluemix Config ###
* Create account on Blumix.net
* Install cf-utils from their downloads section
* Add a swift container from Create APP - button
* Add a CloudantNoSQL DB from Create Service
* Connect the two of them
* From command line(into the root of the project : FoodTruck/FoodTruckAPI):

```sh
$ cf login
```
```sh
$ cf push
```

... And wait... about 10 to 15 minutes for the app to start
